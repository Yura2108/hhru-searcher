from TGBot import TGBot
import os


def main():
    help_text = """
        <strong>Бот создан с целью сбора публичной информации с сайта hh.ru</strong>\n
<i>В результате работы вы получите CSV файл с вакансиями, которые предложены на сайте:
Название вакансии, компания представитель, оплата, ссылка и краткое содержание</i>
        
<strong>Чтобы использовать возможности бота - напишите ему сообщение с запросом </strong>
    """

    # insert yours bot token
    bot = TGBot(os.getenv("BOT_KEY"), help_text)
    bot.start()


if __name__ == '__main__':
    main()
