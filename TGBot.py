from aiogram import Bot, Dispatcher, executor
from aiogram.types import Message

from HHParser import HHParser


class TGBot:
    _bot_instance: Bot = None

    def __init__(self, bot_token, help_text):
        self._bot_token = bot_token
        self._help_text = help_text

    def start(self):
        self._bot_instance = Bot(token=self._bot_token)
        dispatcher = Dispatcher(self._bot_instance)

        self._register_commands(dispatcher)

        executor.start_polling(dispatcher)

    @staticmethod
    def is_not_command(message: Message):
        if message is None:
            raise AttributeError("Message is None")

        return not message.text.startswith("/")

    def _register_commands(self, dispatcher):
        dispatcher.register_message_handler(
            self._start_command_handler,
            commands=["start", "help"]
        )

        dispatcher.register_message_handler(
            self._parse_command_handler,
            self.is_not_command,
            content_types=['text']
        )

    async def _start_command_handler(self, message: Message):
        await message.reply(
            text=self._help_text,
            parse_mode="HTML"
        )

    @staticmethod
    async def _parse_command_handler(self, message: Message):
        search_criteria = message.text

        result_frame = await HHParser(search_criteria).get_data()

        await message.reply_document(
            reply=False,
            document=result_frame.get_file()
        )

        result_frame.clear_data()
