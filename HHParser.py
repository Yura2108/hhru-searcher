import asyncio
import collections

import requests
import bs4

from ResultFrame import ResultFrame


class HHParser:
    BASE_URL = "https://hh.ru/search/vacancy?area=1&fromSearchLine=false"

    # BASE_URL = "https://hh.ru/search/vacancy?L_is_autosearch=false&area=1&clusters=true&enable_snippets=true"

    HEADERS = {'accept': '*/*',
               'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0'}

    def __init__(self, search_criteria):
        self._search_criteria = search_criteria.split()

        self._result_frame = ResultFrame()

    async def get_data(self) -> ResultFrame:
        await self._prepare_session()

        await self._run_process()

        return self._result_frame

    async def _prepare_session(self) -> None:
        self._session = requests.Session()
        self._session.headers = self.HEADERS

    def _get_urls_soup(self):
        request = self.BASE_URL + '&text=' + '+'.join(self._search_criteria)
        response = self._session.get(request + "&page=0")

        if not response.ok:
            return

        soup = bs4.BeautifulSoup(response.content, 'lxml')

        a_pages = soup.find_all('a', attrs={'data-qa': 'pager-page'})

        if a_pages is None or len(a_pages) == 0:
            return

        pages_count = int(a_pages[-1].text)

        for page in range(pages_count):
            if page == 0:
                yield soup

            try:
                response = self._session.get(self.BASE_URL + f"&page={page}")

                yield bs4.BeautifulSoup(response.content, 'lxml')
            except:
                return

    async def _run_process(self):
        for page_soup in self._get_urls_soup():
            for div in page_soup.find_all('div', attrs={'class': 'vacancy-serp-item'}, limit=100):
                title_div = div.find('span', attrs={"class": 'resume-search-item__name'})

                vacancy_title = title_div.text
                company_name = div.find('div', attrs={'class': 'vacancy-serp-item__meta-info-company'}).text

                link = title_div.find('a', href=True)['href']

                try:
                    payment = div.find('span', attrs={'data-qa': 'vacancy-serp__vacancy-compensation'}).text
                except AttributeError:
                    payment = ""

                try:
                    vacancy_responsibility = div.find('div',
                                                    attrs={'data-qa': 'vacancy-serp__vacancy_snippet_responsibility'}).text
                    vacancy_requirements = div.find('div',
                                                    attrs={'data-qa': 'vacancy-serp__vacancy_snippet_requirement'}).text
                except AttributeError:
                    vacancy_responsibility = ""
                    vacancy_requirements = ""

                self._result_frame.insert_data(
                    vacancy_title, company_name, payment, link, vacancy_responsibility, vacancy_requirements
                )
