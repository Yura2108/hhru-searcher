import pandas as pd
import os
import random


class ResultFrame:
    def __init__(self):
        self._columns = ('Вакансия', 'Компания', 'Зарплата', 'Ссылка', 'Условия', 'Требования')
        self._data = list()

        self.file_name = str(random.randint(1000, 10000)) + ".csv"

    def insert_data(self, *args):
        if len(args) != len(self._columns):
            raise AttributeError("Data quantity does not match columns")

        self._data.append(args)

    def get_file(self):
        frame = pd.DataFrame.from_records(columns=self._columns, data=self._data)

        frame.to_csv(self.file_name, sep=";", index=False)

        return open(self.file_name, 'rb')

    def clear_data(self):
        os.remove(self.file_name)

        del self._data
